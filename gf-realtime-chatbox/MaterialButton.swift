//
//  MaterialButton.swift
//  gf-realtime-chatbox
//
//  Created by Daniel Esposito on 5/28/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//

import UIKit

class MaterialButton: UIButton {

    override func awakeFromNib() {
        
        layer.cornerRadius = 5.0
        layer.shadowColor = SHADOW_COLOR
        layer.shadowOpacity = 0.8
        layer.shadowRadius = 5.0
        layer.shadowOffset = CGSizeMake(0.0, 2.0)
        
    }


}
