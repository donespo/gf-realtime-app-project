//
//  JSQViewController.swift
//  gf-realtime-chatbox
//
//  Created by Daniel Esposito on 5/30/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//

import UIKit
import JSQMessagesViewController
class JSQViewController: JSQMessagesViewController {
    
    var incomingBubble: JSQMessagesBubbleImage!
    var outgoingBubble: JSQMessagesBubbleImage!
    var avatars = [String: JSQMessagesAvatarImage]()
    var messages = [JSQMessage]()
    var keys = [String]()
    
    var userName = String()
    var userConnection = Firebase()
    let firebase = Firebase(url: "https://gf-realtime-chatbox.firebaseio.com")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCollectUsers()
        setup()
    }
    
    
    func setupCollectUsers() {
        
        firebase.childByAppendingPath("users").observeSingleEventOfType(FEventType.Value) { (snapshot: FDataSnapshot!) in
            print("Single Event: \(snapshot)")
        }
        
        firebase.childByAppendingPath("users").observeEventType(FEventType.ChildChanged) { (snapshot: FDataSnapshot!) in
            print("Observer: \(snapshot)")
        }
    }
    
    
    func setup() {
        
        self.senderId = senderId
        self.senderDisplayName = senderDisplayName

        print("senderId ' \(senderId) and senderDisplayName = \(senderDisplayName)")
        
        userConnection = Firebase(url: "https://gf-realtime-chatbox.firebaseio.com/users/\(senderId)/isOnline")
        userConnection.onDisconnectSetValue(false)
        
        self.inputToolbar?.contentView?.leftBarButtonItem?.hidden = true
        self.inputToolbar?.contentView?.leftBarButtonItemWidth = 0
        
        let bubbleFactory = JSQMessagesBubbleImageFactory()
        incomingBubble = bubbleFactory.incomingMessagesBubbleImageWithColor(UIColor.jsq_messageBubbleBlueColor())
        outgoingBubble = bubbleFactory.outgoingMessagesBubbleImageWithColor(UIColor.jsq_messageBubbleLightGrayColor())
        
        createAvatar(senderId, senderDisplayName: senderDisplayName, color: UIColor.lightGrayColor(), textColor: UIColor.whiteColor())
        firebase.childByAppendingPath("JSQNode").queryLimitedToLast(50).observeSingleEventOfType(FEventType.Value) { (snapshot: FDataSnapshot!) in
            //            print(snapshot)
            let values = snapshot.value
            
            for value in values as! NSDictionary {
                self.keys.append(value.key as! String)
                if let message = value.value as? NSDictionary {
                    
                    
                    let date = message["date"] as! NSTimeInterval
                    let receivedSenderId = message["senderId"] as! String
                    let receivedDisplayName = message["senderDisplayName"] as! String
                    self.createAvatar(receivedSenderId, senderDisplayName: receivedDisplayName, color: AVATARBGCOLOUR, textColor: UIColor.jsq_messageBubbleBlueColor())
                    
                    let jsqMessage =  JSQMessage(senderId: receivedSenderId, senderDisplayName: receivedDisplayName, date: NSDate(timeIntervalSince1970: date), text: message["message"] as! String)
                    self.messages.append(jsqMessage)
                }
            }
            self.finishReceivingMessageAnimated(true)
        }
        
        firebase.childByAppendingPath("JSQNode").queryLimitedToLast(1).observeEventType(FEventType.ChildAdded) { (snapshot: FDataSnapshot!) in
            
            self.keys.append(snapshot.key)
            if let message = snapshot.value as? NSDictionary {
                
                let date = message["date"] as! NSTimeInterval
                let receivedSenderId = message["senderId"] as! String
                let receivedDisplayName = message["senderDisplayName"] as! String
                self.createAvatar(receivedSenderId, senderDisplayName: receivedDisplayName, color: AVATARBGCOLOUR, textColor: UIColor.jsq_messageBubbleBlueColor())
                
                let jsqMessage =  JSQMessage(senderId: receivedSenderId, senderDisplayName: receivedDisplayName, date: NSDate(timeIntervalSince1970: date), text: message["message"] as! String)
                self.messages.append(jsqMessage)
                if receivedSenderId != self.senderId {
                    JSQSystemSoundPlayer.jsq_playMessageReceivedAlert()
                }
                
            }
            self.finishReceivingMessageAnimated(true)
            
        }
    }
    
    
    func createAvatar(senderId:String, senderDisplayName: String, color: UIColor, textColor: UIColor){
        
        if avatars[senderId] == nil {
            let initial = senderDisplayName.substringToIndex(senderDisplayName.startIndex.advancedBy(min(2, senderDisplayName.characters.count)))
            
            let avatar = JSQMessagesAvatarImageFactory.avatarImageWithUserInitials(initial, backgroundColor: color, textColor: textColor, font: UIFont.systemFontOfSize(14), diameter:UInt(kJSQMessagesCollectionViewAvatarSizeDefault))
            avatars[senderId] = avatar
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func didPressSendButton(button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: NSDate!) {
//        let message = JSQMessage(senderId: senderId, displayName: senderDisplayName, text: text)
        
        firebase.childByAppendingPath("JSQNode").childByAutoId().setValue(["message":text, "senderId": senderId, "senderDisplayName": senderDisplayName, "date": date.timeIntervalSince1970, "messageType": "txt"])
//        messages.append(message)
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        finishSendingMessage()
    }

//    MARK - Delegates
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageData! {
        
        return messages[indexPath.row]
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageBubbleImageDataSource! {
        
        let message = messages[indexPath.row]
        
        if message.senderId == senderId {
            return outgoingBubble
        }
        
        return incomingBubble
        
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageAvatarImageDataSource! {
        let message = messages[indexPath.row]
        
        return avatars[message.senderId]
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAtIndexPath: indexPath) as! JSQMessagesCollectionViewCell
        
        let message = messages[indexPath.row]
        
        if message.senderId == senderId {
            cell.textView?.textColor = UIColor.blackColor()
        } else {
            cell.textView?.textColor = UIColor.whiteColor()
        }
        
        cell.textView?.linkTextAttributes = [NSForegroundColorAttributeName:(cell.textView?.textColor)!]
        
        return cell
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAtIndexPath indexPath: NSIndexPath!) -> NSAttributedString! {
        
        let message = messages[indexPath.row]
        
        if indexPath.row <= 1 {
            return NSAttributedString(string: message.senderDisplayName)
        }
        
        return nil
    }
    
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    
    
}
