//
//  AppDelegate.swift
//  gf-realtime-chatbox
//
//  Created by Daniel Esposito on 5/26/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var firebase = Firebase(url: "https://gf-realtime-chatbox.firebaseio.com/")
    


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        if let uid = self.firebase.authData?.uid {
            self.firebase.childByAppendingPath("users").childByAppendingPath(uid).updateChildValues(["isOnline": false])
        }
    }

    func applicationWillEnterForeground(application: UIApplication) {
        if let uid = self.firebase.authData?.uid {
            self.firebase.childByAppendingPath("users").childByAppendingPath(uid).updateChildValues(["isOnline": true])
        }
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

