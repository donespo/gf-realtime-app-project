//
//  MaterialTextField.swift
//  gf-realtime-chatbox
//
//  Created by Daniel Esposito on 5/28/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//
import UIKit

class MaterialTextField: UITextField {
    
    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10);
    
    override func awakeFromNib() {
        layer.cornerRadius = 4.0
        layer.borderColor = BORDER_COLOR
        layer.borderWidth = 1.0
    }
    
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func placeholderRectForBounds(bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
}



