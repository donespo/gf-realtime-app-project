//
//  MaterialButton.swift
//  GETFiT-Showcase
//
//  Created by Daniel Esposito on 5/20/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//

import UIKit

class MaterialButtonNoShadow: UIButton {

    override func awakeFromNib() {
        
        layer.cornerRadius = 5.0
        
    }
    
    

}
