//
//  MessageVC.swift
//  gf-realtime-chatbox
//
//  Created by Daniel Esposito on 5/30/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//

import UIKit

class CreateMessageVC: UIViewController {
    
    @IBOutlet weak var messageTextField: UITextField!
    var stacksStatusOn = true
    var stacksStatusOff = false
    var onMessageAvailable: ((data:String) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func sendnewMessage(sender: AnyObject) {
        
        if let text = messageTextField.text {
        
        onMessageAvailable!(data:text)
            
        }
        messageTextField.text = ""
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func cancelNewMessage(sender: AnyObject) {
        messageTextField.text = ""
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }

}
