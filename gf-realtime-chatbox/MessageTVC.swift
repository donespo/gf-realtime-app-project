//
//  PostTVC.swift
//  gf-realtime-chatbox
//
//  Created by Daniel Esposito on 5/30/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//

import UIKit

struct User {
    let uid: String?
    let name: String?
}

//struct Messages {
//    let message: String?
//    let uid: String?
//}

class MessageTVC: UITableViewController {
    var firebase = Firebase(url: "https://gf-realtime-chatbox.firebaseio.com/")
    var childAddedhandler = FirebaseHandle()
    var listOfMessages = NSMutableDictionary()
    let uid = String?()
    
    @IBAction func addNewMessage(sender: AnyObject) {
        
    }
    
    @IBAction func logoutUser(sender: AnyObject) {
        let uid = firebase.authData.uid
        firebase.childByAppendingPath("users").childByAppendingPath(uid).updateChildValues(["isOnline": false])
//        firebase.unauth()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        childAddedhandler = firebase.childByAppendingPath("posts").observeEventType(.Value, withBlock: { (snapshot: FDataSnapshot!) in
            self.firebaseUpdate(snapshot)
        })
        
        childAddedhandler = firebase.observeEventType(.ChildChanged, withBlock: { (snapshot: FDataSnapshot!) in
            self.firebaseUpdate(snapshot)
        })
    }
    
    func firebaseUpdate(snapshot: FDataSnapshot) {
        
        if let newMessages = snapshot.value as? NSDictionary{
            print(newMessages)
            for newMessage in newMessages {
                let key = newMessage.key as! String
                let messageExist =  (self.listOfMessages[key] != nil)
                if !messageExist {
                    self.listOfMessages.setValue(newMessage.value, forKey:key)
                }
            }
        }
        dispatch_async(dispatch_get_main_queue()){[unowned self] in
            self.tableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return listOfMessages.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("messageCell", forIndexPath: indexPath)
        let arrayOfKeys = listOfMessages.allKeys
        let key = arrayOfKeys[indexPath.row]
        let value = listOfMessages[key as! String]
        cell.textLabel?.text = (value as! NSDictionary)["message"] as? String
        
        // Configure the cell...

        return cell
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */


    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let messageController = segue.destinationViewController as? CreateMessageVC {
            messageController.onMessageAvailable = {[weak self]
                (data) in
                if let weakSelf = self {
                    weakSelf.receiveMessageToSendToFirebase(data)
                }
            }
        }
    }
    
    func receiveMessageToSendToFirebase(data: String) {
        let details = ["message": data, "sender":firebase.authData.uid]
        firebase.childByAppendingPath("posts").childByAutoId().setValue(details)
        
    }
}
