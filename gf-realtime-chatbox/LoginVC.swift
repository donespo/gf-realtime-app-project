//
//  LoginVC.swift
//  gf-realtime-chatbox
//
//  Created by Daniel Esposito on 5/27/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    //FIELDS
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var usernameField: UITextField!
    //LABELS
    @IBOutlet weak var loginStatusLabel: UILabel!
    //STACKS
    @IBOutlet weak var loginSignupBtnStack: UIStackView!
    @IBOutlet weak var emailPasswordFieldStack: UIStackView!
    @IBOutlet weak var usernameSubmitStack: UIStackView!

    var firebase = Firebase(url: "https://gf-realtime-chatbox.firebaseio.com/")
    var newUser = false
    var username = String()
    var uid = String()
    var loader = LoadingOverlay()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.usernameSubmitStack.hidden = true
        
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
//        logoutTemp() //To create new users, temporarily enable this. Dont forget to disable it after successful registration.
        let localUid = NSUserDefaults.standardUserDefaults().valueForKey("uid") as! String
        if let uid = firebase.authData?.uid {
            if firebase.authData != nil {
                if firebase.authData.uid == localUid {
                    firebase.childByAppendingPath("users").childByAppendingPath(uid).updateChildValues(["isOnline": true])
                    //            print(firebase.authData)
                    self.retrieveUsername()
                }
            }
        }
    }
    
    func logoutTemp() {
        firebase.unauth()
        writeLoginLocally("0")
    }
    
    @IBAction func loginButtonPressed(sender: UIButton) {
       
        if checkFields() {
            loader.showOverlay(view)
            firebase.authUser(emailTextField.text, password: passwordField.text) { (error: NSError!, authData: FAuthData!) in
                
                if error != nil {
                    
                    self.checkErrorCode(error.code)
                    self.displayMessage(error)
                    
                } else {
                    self.uid = authData.uid
                    self.writeLoginLocally(authData.uid)
                    self.firebase.childByAppendingPath("users").childByAppendingPath(self.uid).updateChildValues(["isOnline": true])
                    self.resetTextFields()
                    self.retrieveUsername()
                }
                
                self.loader.hideOverlayView()
            }
            
        }
    }
    
    func retrieveUsername() {
        
        self.firebase.childByAppendingPath("users").childByAppendingPath(firebase.authData.uid).observeSingleEventOfType(FEventType.Value) { (snapshot: FDataSnapshot!) in
//            print(snapshot)
            self.username = (snapshot.value as! NSDictionary)["name"] as! String
            self.performSegueWithIdentifier(SEGUE, sender: self)
        }
    }
    
    func writeLoginLocally(authData: String) {
        
        NSUserDefaults.standardUserDefaults().setValue(authData, forKey: "uid")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == SEGUE {
            if let viewController = segue.destinationViewController as? JSQViewController {
            
                viewController.senderId = self.firebase.authData.uid
                viewController.senderDisplayName = self.username
            }
        }
    }
    
    @IBAction func signupButtonPressed(sender: UIButton) {
        if checkFields() {
            loader.showOverlay(view)
            firebase.createUser(emailTextField.text, password: passwordField.text) { (error: NSError!) in
                self.loader.showOverlay(self.view)
                if error != nil {
//                    print(error.description)
                    self.checkErrorCode(error.code)
                    self.displayMessage(error)
                } else {
//                    print("DEV: New user was created")
//                    self.loginStatusLabel.text = "Account was created and looged in successfully!"
                    self.createUser()
                }
                self.loader.hideOverlayView()
            }
        }
    }
    
    @IBAction func submitUsernamePressed() {
        createUsername()
        
    }
    
    
    
    func createUser() {
        if checkFields() {
            loader.showOverlay(view)
            firebase.authUser(emailTextField.text, password: passwordField.text) { (error: NSError!, authData: FAuthData!) in
                if error != nil {
                    
                    self.checkErrorCode(error.code)
                    self.displayMessage(error)
                   
                } else {
                    self.uid = authData.uid
                    self.loginStatusLabel.textColor = NOTIF_TEXT
                    self.loginStatusLabel.text = "Account created!\n Please choose a usename."
                    self.loginSignupBtnStack.hidden = true
                    self.emailPasswordFieldStack.hidden = true
                    self.usernameSubmitStack.hidden = false
                }
                self.loader.hideOverlayView()
            }
        }
    }
    
    func createUsername() {
//        loader.showOverlay(view)
        if checkUsernameField() {
            username = usernameField.text!
            firebase.childByAppendingPath("users").childByAppendingPath(uid).setValue(["isOnline": true, "name": username, "userid" : makeRndNumber()])
            resetTextFields()
            self.writeLoginLocally(self.uid)
            self.loginSignupBtnStack.hidden = false
            self.emailPasswordFieldStack.hidden = false
            self.usernameSubmitStack.hidden = true
            self.loginStatusLabel.text = ""
            self.retrieveUsername()
        }
//        loader.hideOverlayView()
    }
    
    func checkUsernameField() -> Bool {
        if !usernameField.text!.isEmpty {
            return true
        } else {
            loginStatusLabel.textColor = ERROR_TEXT
            loginStatusLabel.text = "Please supply a username"
            return false
        }
    }
    
    func checkFields() -> Bool {
        if !emailTextField.text!.isEmpty && !passwordField.text!.isEmpty {
            return true
        } else {
            loginStatusLabel.textColor = ERROR_TEXT
            loginStatusLabel.text = "Empty email/password field was found"
            return false
        }
    }
    
    func checkErrorCode(code: Int) -> String{
        if code == -6 {
            let msg = "Invalid Password was provided. Please try again"
            return msg
        } else if code  == -8 {
            let msg = "Email account doesn't exist. First time user, please SignUp first"
            return msg
        } else if code == -5 {
            let msg = "The specified email address is invalid."
            return msg
        } else if code == -9 {
            let msg = "The specified email address is already in use."
            return msg
        }
        let msg = "Unknow error occured. Please try again"
        return msg
    }
    
    func displayMessage(error: NSError) {
        let msgTitle = "Error"
        let alert = UIAlertController(title: msgTitle, message:checkErrorCode(error.code), preferredStyle: .Alert)
        let actionOk = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(actionOk)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func createUsernamePopup() {
        var usernameTextField: UITextField?
        let msgTitle = "Enter a Username"
        let bodyMsg = "Please enter a username for your new account:"
        let usernameEntry = UIAlertController(title: msgTitle, message:bodyMsg, preferredStyle: .Alert)
        let actionOk = UIAlertAction(title: "Ok", style: .Default) { (UIAlertAction) in
            
            if let user = usernameTextField?.text {
                print(user)
            }
        }
        usernameEntry.addAction(actionOk)
        usernameEntry.addTextFieldWithConfigurationHandler { (username: UITextField) in
        usernameTextField = username
        }
        
        self.presentViewController(usernameEntry, animated: true, completion: nil)
        
    }
    
    func resetTextFields() {
        self.emailTextField.text = ""
        self.passwordField.text = ""
        self.usernameField.text = ""
    }
    
    func makeRndNumber() -> String {
        let random = arc4random_uniform(10000)
        let random2 = arc4random_uniform(10000)
        let combo = "GFCB\(random)\(random2)"
        return combo
    }
}

