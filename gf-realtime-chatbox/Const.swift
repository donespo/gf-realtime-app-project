//
//  Const.swift
//  gf-realtime-chatbox
//
//  Created by Daniel Esposito on 5/28/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//

import Foundation
import UIKit


//let SHADOW_COLOR: CGFloat = 157.0/255.0
let SHADOW_COLOR = UIColor(red: 162/255, green: 162/255, blue: 162/255, alpha: 0.5).CGColor
let BORDER_COLOR = UIColor(red: 162/255, green: 162/255, blue: 162/255, alpha: 0.1).CGColor
let AVATARBGCOLOUR = UIColor(red: 85/255, green: 160/255, blue: 249/255, alpha: 0.1)
let ERROR_TEXT = UIColor.redColor()
let SUCCESS_TEXT = UIColor.greenColor()
let NOTIF_TEXT = UIColor.blueColor()
let SEGUE = "segueJSQ"