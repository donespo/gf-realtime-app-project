//
//  ViewController.swift
//  gf-realtime-chatbox
//
//  Created by Daniel Esposito on 5/26/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var textMessage: UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var textField: UITextField!
    
    
    
    let firbase = Firebase(url: "https://gf-realtime-chatbox.firebaseio.com/")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submitMsgPressed(sender: UIButton) {

    }
    
    func makeRndNumber() -> String {
        let random = arc4random_uniform(1000000000)
        let random2 = arc4random_uniform(1000000000)
        let combo = "\(random)\(random2)"
        return combo
    }
}

